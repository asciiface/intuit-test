name             'test'
maintainer       'Charles'
maintainer_email 'test@test.test'
license          'Apache 2.0'
description      'Installs/Configures test'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

def supported_operating_systems
  %w( debian ubuntu )
end

supported_operating_systems.each { |os| supports os }

recipe 'test::default', 'Does nothing'

depends 'apt'
depends 'java'