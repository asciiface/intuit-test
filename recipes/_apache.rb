#
# Cookbook Name:: test
# Recipe:: _apache
#

package "apache2" do
	action :install
end

# enable apache modules
execute "a2enmod proxy" do
  action :run
  notifies :restart, "service[apache2]", :delayed
  not_if { ::File.exists?("/etc/apache2/mods-enabled/proxy.load")}
end
execute "a2enmod proxy_ajp" do
  action :run
  notifies :restart, "service[apache2]", :delayed
  not_if { ::File.exists?("/etc/apache2/mods-enabled/proxy_ajp.load")}
end
execute "a2enmod proxy_http" do
  action :run
  notifies :restart, "service[apache2]", :delayed
  not_if { ::File.exists?("/etc/apache2/mods-enabled/proxy_http.load")}
end

# remove default apache config
execute 'a2dissite default' do
	action :run
	only_if { ::File.exists?("/etc/apache2/sites-enabled/000-default")}
end

execute "rm /etc/apache2/sites-enabled/000-default" do
  action :run
  notifies :restart, "service[apache2]", :delayed
  only_if { ::File.exists?("/etc/apache2/sites-enabled/000-default")}
end

package "tomcat7" do
	action :install
end

directory "/var/log/apache2" do
	mode 00755
end

service "apache2" do
	action :start
end

service "tomcat7" do
	action :start
end

cookbook_file '/etc/tomcat7/server.xml' do
	source 'server.xml'
	owner 'root'
	group 'tomcat7'
	action :create
	notifies :restart, "service[tomcat7]", :delayed
end

cookbook_file '/etc/apache2/sites-available/testweb' do
	source 'testweb'
	owner 'root'
	group 'root'
	action :create
end

# enable the site!
execute 'a2ensite testweb' do
	action :run
	notifies :restart, "service[apache2]", :delayed
end

