#
# Cookbook Name:: _init
# Recipe:: default
#

# install/configure apache & tomcat
include_recipe "test::_init"

# get unzip, curl, vim

%w{unzip curl vim}.each do |pkg|
	package pkg do
		action :install
	end
end

# extract the application and notify the deploy
execute 'extract_testweb' do
	command 'unzip -p testweb.zip testweb/testweb.war > testweb.war'
	cwd '/var/lib/tomcat7/webapps/'
	action :nothing
end

# download the application
remote_file '/var/lib/tomcat7/webapps/testweb.zip' do
	source 'http://www.cumulogic.com/download/Apps/testweb.zip'
	action :create
	notifies :run, 'execute[extract_testweb]', :immediately
end

# Future
'''
If I had more time, and less restrictions, I would use the tomcat and 
apache LWRPs to do a lot of this and it would be cleaner.
It would also use templates + attributes instead of static 
cookbook_files, which is kind of ugly and was done quickly for
the exercise.
'''